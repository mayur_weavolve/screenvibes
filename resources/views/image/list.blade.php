@extends('layouts.admin')

@section('content')
<div class="m-content">
	<div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30" role="alert">
	</div>
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						Image List
					</h3>
				</div>
			</div>
			<!-- <div class="m-portlet__head-tools">
			<ul class="m-portlet__nav">
				<li class="m-portlet__nav-item">
					<a href="" class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
						<span>
							<i class="la la-plus"></i>
						    <span>Store Add</span>
						</span>
					</a>
				</li>
				<li class="m-portlet__nav-item"></li>
				<li class="m-portlet__nav-item">

			</ul>
	        </div> -->
	</div>
	<div class="m-portlet__body">
		<!-- <div class = "row">
			<div class = "col-md-12">
				<form action = "" method = "get">
				<div class="row">
					<div class = "col-md-10 text-right">
						<input type="search" name="search"class="form-control" placeholder="Search"><br>
					</div>
					<div class="col-md-2 text-right">
							<button type = "submit" class ="btn btn-primary">Search</button><br>
					</div>
				</div>
				</form>
			</div>
		</div> -->
		<table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
			<thead>
				<tr>
					<!-- <th> ID</th> -->
                    <th>Id</th>
                    <th>User Name</th>
                    <th>Image</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
			@if(count($images) == 0)
			<tr><td colspan="8"  style="text-align:center;">No record found</td>
			@endif
			<?php foreach ($images as $image) { ?>
			<tr>
				<td> <?php echo $image->id ;?> </td>
                <td>{{ $image->user->name}}</td>
                <td><a href = "{{route('image-view',[$image->id])}}">{{ $image->image}}</td>
				<td>
						<!-- <a href=""  title="Edit"><i class="m-menu__link-icon fa fa-edit"></i></a>  &nbsp;&nbsp;
						<a href="" onclick="return confirm('Are you sure you want to delete this Store?');"  title="Delete"><i class="m-menu__link-icon fa fa-trash-alt"></i></a> &nbsp;&nbsp;
						<a href=""  title="Detail"><i class="m-menu__link-icon fa fa-info"></i></a> -->
				</td>
				</tr>
			<?php } ?>
			</tbody>
		</table>
	</div>
</div>
</div>
</div>
</div>
@endsection
