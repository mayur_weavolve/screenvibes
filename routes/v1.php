<?php

Route::post('login', 'Api\UserController@login');

Route::group(['middleware' => 'auth:api'], function(){
    Route::post('image/upload', 'Api\ImageController@image_upload');
    Route::post('video/upload', 'Api\VideoController@video_upload');
});