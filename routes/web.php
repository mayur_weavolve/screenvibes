<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('welcome', 'HomeController@index')->name('welocome');

Route::group(['middleware' => 'auth'], function(){
    Route::get('welcome','HomeController@index')->name('welcome');
    Route::get('image/list','ImageController@image')->name('image');
    Route::get('image/view/{id}','ImageController@image_view')->name('image-view');
    Route::get('video/list','VideoController@video')->name('video');
});
