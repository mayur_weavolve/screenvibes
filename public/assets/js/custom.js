
 /*== header sticky ==*/
 window.onscroll = function () {
     myFunction()
 };
 var header = document.getElementById("loginheader");
 var sticky = header.offsetTop;

 function myFunction() {
     if (window.pageYOffset > sticky) {
         header.classList.add("sticky");
     }
     else {
         header.classList.remove("sticky");
     }
 }
 /*== Backgroud Slider ==*/
 $(document).ready(function () {
    $(".register-area").hide(); 
    $(".loading").hide(); 
    $(".login-line").hide(); 
    $(".content-area").fadeOut();
    $(".search-text").hide();

     $('.ld-slider').owlCarousel({
         loop: true
         , margin: 0
         , nav: false
         , touchDrag: false
         , mouseDrag: false
         , autoplay: true
         , animateOut: 'fadeOut'
         , animateIn: 'fadeIn'
         , dots: false
         , responsive: {
             0: {
                 items: 1
             }
             , 600: {
                 items: 1
             }
             , 1000: {
                 items: 1
             }
         }
     });

 });

 function showreregister(){
    //  alert('asd');
    setTimeout(function(){
        $(".register-area").show(); 
        $(".login-line").show(); 
        $(".login-area").hide(); 
        $(".signup-line").hide(); 
        $(".dropdown").addClass("show");
        $(".dropdown-menu").addClass("show");
    }, 100);
}
function showlogin(){
    setTimeout(function(){
        $(".register-area").hide(); 
        $(".login-line").hide(); 
        $(".login-area").show(); 
        $(".signup-line").show(); 
        $(".dropdown").addClass("show");
        $(".dropdown-menu").addClass("show");
    }, 100);
}

function getPublicPlan(){
    var search = $("#search").val();
    if(search != ''){
        $(".loading").show(); 
        $(".content-area").show();
        $("content-area").fadeIn(3000);
        $(".searchbox").addClass('smallspace');
        $.ajax({
            url: '/getfrontpublicplan',
            type: "post",
            data: { 'search' : search },
            success: function (response){
                $(".loading").hide(); 
                if(response=='error'){
                        alert('Public Travel Plan not found');
                    }else{
                        $('.content-area').html(response);
                        // $('.search-text').html('Showing Vacations for <span>'+search+'</span>');
                        // $(".search-text").show();
                        //alert('Travel Plan Copied successfully');
                        //window.location.href  = BASE +'/travelplan';
                    }
            },
            error : function (response){
                $(".loading").hide(); 
            }
        });
    }else{
        alert('Please enter the value in search field');
    }
}

