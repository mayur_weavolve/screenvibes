<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; 
use Validator;
use Config;
use App\Video;

class VideoController extends Controller
{
    public function video_upload(Request $request)
    {
        $data = $request->all();
        $user = Auth::user();
        $data['video'] = $request->file('video');
        if ($request->file('video')==null){  
            return response()->json(['status'=>false,'statusCode' => '400',"message" =>"Please Select video First....!",'data' =>  null]);      
        }
        else{
            $data['user_id'] = $user->id;
            $data['video'] = $request->file('video')->hashName();
            $request->file('video')
            ->store('video', ['disk' => 'public']);
            $data['url'] = Config::get('constants.video').$data['video']; 
            $upload = Video::create($data);       
        }
        return response()->json(['status'=>true,'statusCode' => '200',"message" =>"video upload Successfully....!",'data' =>  $data]);

    }
}
