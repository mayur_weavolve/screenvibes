<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; 
use Illuminate\Support\Facades\Hash;
use Validator;
use App\User;

class UserController extends Controller
{
    public function login(Request $request)
    {
        $data = $request->all();
        $user = User::where('google_id',$data['google_id'])->first();
        if($user)
        {
            $success['token'] =  $user->createToken('MyApp')->accessToken;
            $success['user_id'] =  $user->id;
            $success['name'] =  $user->name;
            $success['email'] =  $user->email;
            $success['google_id'] =  $user->google_id;
            return response()->json(['status'=> true,'statusCode' => '200' ,"message" =>"Login Sucessfully",'data' =>$success]);
        }
        $validator = Validator::make($request->all(), [ 
            'name' => 'required', 
            'google_id' => 'required', 
            'email' => 'required|email|unique:users', 
        ]);
        if ($validator->fails()) { 
            return response()->json(['status'=>false,'statusCode' => '400' ,"message" =>"Somthing Wrong Please try again",'data' => $validator->errors()]);
        }
        $data['password'] = bcrypt($data['password']);
        $user = User::create($data);  
        $success['token'] =  $user->createToken('MyApp')->accessToken;
        $success['user_id'] =  $user->id;
        $success['name'] =  $user->name;
        $success['email'] =  $user->email;
        $success['google_id'] =  $user->google_id;
        return response()->json(['status'=> true,'statusCode' => '200' ,"message" =>"User Register Successfully....!",'data' =>$success]); 
    }
}
