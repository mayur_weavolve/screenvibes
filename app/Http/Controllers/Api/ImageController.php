<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; 
use Validator;
use Config;
use App\Image;

class ImageController extends Controller
{
    public function image_upload(Request $request)
    {
        $data = $request->all();
        $user = Auth::user();
        $data['image'] = $request->file('image');
        if ($request->file('image')==null){  
            return response()->json(['status'=>false,'statusCode' => '400',"message" =>"Please Select Image First....!",'data' =>  null]);      
        }
        else{
            $data['user_id'] = $user->id;
            $data['image'] = $request->file('image')->hashName();
            $request->file('image')
            ->store('image', ['disk' => 'public']);
            $data['url'] = Config::get('constants.image').$data['image']; 
            $upload = Image::create($data);       
        }
        return response()->json(['status'=>true,'statusCode' => '200',"message" =>"Image upload Successfully....!",'data' =>  $data]);

    }
}
