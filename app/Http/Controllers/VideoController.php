<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Video;

class VideoController extends Controller
{
    public function video()
    {
        $list = Video::get();
        $data = [];
        $data['videos'] = $list;
        return view('video.list',$data);
    }
}
