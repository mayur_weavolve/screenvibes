<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Image;

class ImageController extends Controller
{
    public function image()
    {
        $list = Image::get();
        $data = [];
        $data['images'] = $list;
        return view('image.list',$data);
    }
    public function image_view($id)
    {
        $list = Image::where('id',$id)->first();
        return view('image.image-view',['image'=>$list]);
    }
}
